# Mender.io for miniserver update system

## Testing

I'm testing the [On premise installation][1]. This howto help with the
instalation of a demo server using docker-compose and a client in a rpi3 using
a prebuild image.

### Server

Ones the server is installed you have to add a device. It ask to install the
client in the device:

```
sudo bash -c 'wget https://d1b0l86ne08fsf.cloudfront.net/2.2.0/dist-packages/debian/armhf/mender-client_2.2.0-1_armhf.deb && \
DEBIAN_FRONTEND=noninteractive dpkg -i --force-confdef --force-confold mender-client_2.2.0-1_armhf.deb && \
DEVICE_TYPE="raspberrypi3" && \
mender setup \
  --device-type $DEVICE_TYPE \
  --quiet --demo --server-ip 192.168.1.123 && \
systemctl restart mender-client'
```

Ones the client is installed it announces to the server and you can create
releases (group of artifacts) to install in the clients. The creating a deployment the
release is installed to the clients.

This works fine with a raspberrypi3. We dont want to use a server so is not an
option.

## Standalone

There is a mode to the mender client to work without a server [2].

The process steps are:

* Download
* ArtifactInstall
* ArtifactCommit
* ArtifactRollback
* ArtifactFailure

The flow depends on the output of each step.

### Modifiying the image

The only step to convert the "On premise installation" image to be standalone
is to disable the systemd mender service.

### Create an artifact from the golden image

Using the running system as golden image I installed a package and make and
snapshop dump:

```
sudo mender snapshot dump | ssh me@192.168.1.123 /bin/sh -c 'cat > $HOME/root-part.ext4'
```

and then create the artifact on the host:

```
mender-artifact write rootfs-image -f root-part.ext4 -n install_ipython3 -o snapshot_install_ipython3.mender -t raspberrypi
```

By default the image must be sined [3]. The public key must be on the device
and the key path configured on ```/etc/mender/mender.conf```.

### Install the artifact

For testing of the artifact I created another SD card with the base image and
install with:

```
sudo mender install http://192.168.1.123:8090/snapshot_install_ipython3-signed.mender
```

Using the image published by http.

### Comments 

Pros:
* Easy to install. We can use as base image the rpi3 image of the demo and
  build from that. Or build a debian with the instructions.
* It works. The tested standalone update was succesfull.

Cons:
* The documentation is note the best, is not easy to find things.
* I dont like the tools, maybe is because of the documentation but I miss
  functionalities like to see the current state of the image or the diff with
  the base image. I do not know if there is a way to change of partition in
  use.
* The rollback is not of applied artifacts. Is only of non commited artifact.


[1]: https://docs.mender.io/2.4/getting-started/on-premise-installation
[2]: https://docs.mender.io/2.4/architecture/standalone-deployments
[3]: https://docs.mender.io/2.4/artifacts/signing-and-verification
